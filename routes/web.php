<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('coffees', 'CoffeeController');

Route::get('/home/coffeesJson', 'CoffeeController@coffeesJson');

Route::post('/home/addtodb', 'CoffeeController@addtodb')->name('addtodb');
