<?php

namespace App\Http\Controllers;

use App\Coffee;
use Illuminate\Http\Request;

class CoffeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('coffee.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
    'title' => 'required',
    'price' => 'required',
  ]);

      $coffee = new Coffee();
      $coffee->title = $request->title;
      $coffee->price = $request->price;
      $coffee->save();
      return redirect()->route('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Coffee  $coffee
     * @return \Illuminate\Http\Response
     */
    public function show(Coffee $coffee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Coffee  $coffee
     * @return \Illuminate\Http\Response
     */
    public function edit(Coffee $coffee)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Coffee  $coffee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Coffee $coffee)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Coffee  $coffee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Coffee $coffee)
    {
        $coffee->delete();
    }
    public function coffeesJson()
    {   $coffee = Coffee::all();

      return $coffee->toJson();
    }

    public function addtodb(Request $request)
    {
      $coffee = new Coffee();
      $coffee->title = $request->title;
      $coffee->price = 10;
      $coffee->save();
    }
}
