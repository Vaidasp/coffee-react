@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard
                  <a class=" btn btn-primary" href="{{ route('coffees.create') }}">
            + Add Coffee
            </a>
                </div>

                <div class="panel-body">
                    <div id="coffee">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
