@extends('layouts.app')

@section('content')
  <div class="container">
    @if (isset($coffee))
      <h1>Coffee edit</h1>
    @else
      <h1>Coffee create</h1>
    @endif
    @if (count($errors) > 0)
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="row">
          <div class="col-md-12">
            @if (isset($coffee))
              {!! Form::model($coffee, ['route' => ['coffees.update', $coffee->id],'method' => 'put']) !!}
            @else
              {!! Form::open([
                'route' => 'coffees.store',
                'method' =>'post'
                ]) !!}
              @endif
              <div class="form-group">
                <label for="title">Coffee title:</label>
                {!!Form::text('title',null,['class'=>"form-control",'placeholder'=>'Title'])!!}
                <label for="price">Coffee price:</label>
                {!!Form::number('price',null,['class'=>"form-control",'placeholder'=>'Price'])!!}
              </div>

              <div class="btn-group">
                @if (isset($coffee))

                  {!!Form::submit('Save',['class' => 'btn btn-primary'])!!}
                  {!! Form::close() !!}
                @else
                  {!!Form::submit('Submit',['class' => 'btn btn-primary'])!!}
                  {!! Form::close() !!}
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  @endsection
