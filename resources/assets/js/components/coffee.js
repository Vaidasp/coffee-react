import React, {Component} from 'react';
import ReactDOM from 'react-dom';




class NameForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {value: ''};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {
    // alert('A name was submitted: ' + this.state.value);
    event.preventDefault();
    var title = this.state.value;
    $.ajax({
      url: '/home/addtodb',
      method: 'post',
      data: {
        _token: window.Laravel.csrfToken,
        title,
      }
    });
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          Title:
          <input type="text" value={this.state.value} onChange={this.handleChange} />
        </label>
        <input type="submit" value="Submit" />
      </form>
    );
  }
}


class Coffee extends Component {
  constructor(props) {
    super(props);

    this.state = {
      coffees: [],
      lenght: 0
    }
  }
  componentDidMount() {
    fetch('/home/coffeesJson').then(response => {
      return response.json();
    }).then(coffees => {
      this.setState({coffees});
    });
  }

  delete(coffee) {
    $.ajax({
      url: 'coffees/' + coffee.id,
      method: 'delete',
      data: {
        _token: window.Laravel.csrfToken
      }
    });
    const newState = this.state.coffees;
    if (newState.indexOf(coffee) > -1) {
      newState.splice(newState.indexOf(coffee), 1);
      this.setState({coffees: newState})
    }

  }

  renderCoffees() {
    return this.state.coffees.map(coffee => {
      return (
        <div key={coffee.id} className="col-md-3 coffee">
          <img className="img-responsive" src="http://unsplash.it/300"/>
          <h3>{coffee.title}</h3>
          <h3>Price: {coffee.price}
            &euro;</h3>
          <button className="btn btn-xs btn-danger" onClick={this.delete.bind(this, coffee)}>Delete</button>
        </div>
      );
    })
  }

  render() {
    return (
      <div className="row">
        <NameForm />
        {this.renderCoffees()}
      </div>
    );
  }
}

export default Coffee;

if (document.getElementById('coffee')) {
  ReactDOM.render(
    <Coffee/>, document.getElementById('coffee'));
}
